<?php

use Illuminate\Database\Seeder;
use App\Comment;
use App\User;
use App\Content;
use App\Wiki;
class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_ids = User::pluck('id')->all();
        $wiki_ids = Wiki::pluck('id')->all();
        $faker = app(Faker\Generator::class);
        for ($i = 0; $i<=1000; $i++) {
            $comment = Comment::create([
                'commentable_id' => $faker->randomElement($wiki_ids),
                'user_id' => $faker->randomElement($user_ids),
                'commentable_type' => 'App\Wiki',
            ]);
            $comment->content()->updateOrCreate(['contentable_id' => $comment->id], [
                'body' => $faker->sentence,
            ]);
            $comment->user->refreshCache();
            $comment->commentable->refreshCache();
        }
    }
}
