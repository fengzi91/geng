<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = app(Faker\Generator::class);
        $users = factory(User::class)
            ->times(100)
            ->make()
            ->each(function($user) {
                $created_at = Carbon::now()->subMonths(3)->addDays(rand(1,30));
                $updated_at = $created_at->addDays(rand(3,15));
                $user->created_at = $created_at;
                $user->updated_at = $updated_at;
            });

        // 让隐藏字段可见，并将数据集合转换为数组
        $user_array = $users->makeVisible(['password', 'remember_token'])->toArray();

        // 插入到数据库中
        User::insert($user_array);

        // 单独处理第一个用户的数据
        $user = User::find(1);
        $user->name = 'fengzi91';
        $user->email = 'fengzi91@vip.qq.com';
        $user->password = Hash::make('123456');
        $user->save();
        $user->assignRole('Founder');

        // 将 2 号用户指派为『管理员』
        User::find(2)->assignRole('Administrator');
        User::find(3)->assignRole('Editor');
    }
}
