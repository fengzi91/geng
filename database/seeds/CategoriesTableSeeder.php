<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = app(Faker\Generator::class);

        $initCategories = [
            [
                'title' => '网络',
                'slug' => 'wang-luo',
                'description' => $faker->sentence(),
                'created_at' => $faker->dateTimeThisMonth(),
                'updated_at' => now(),
            ],
            [
                'title' => '电竞',
                'slug' => 'dian-jing',
                'description' => $faker->sentence(),
                'created_at' => $faker->dateTimeThisMonth(),
                'updated_at' => now(),
            ],
            [
                'title' => '直播',
                'slug' => 'zhi-bo',
                'description' => $faker->sentence(),
                'created_at' => $faker->dateTimeThisMonth(),
                'updated_at' => now(),
            ],
            [
                'title' => '其它',
                'slug' => 'other',
                'description' => $faker->sentence(),
                'created_at' => $faker->dateTimeThisMonth(),
                'updated_at' => now(),
            ],
        ];
        Category::insert($initCategories);
        // 取出所有顶级栏目

        $category_ids = Category::pluck('id')->toArray();
        foreach($category_ids as $category_id) {
            $children = factory(Category::class)
            ->times(rand(3,8))
            ->make()
            ->each(function ($category, $index)
            use ($category_id){
                $category->category_id = $category_id;
            });
            Category::insert($children->toArray());
        }

    }
}
