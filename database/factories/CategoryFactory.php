<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $sentence = $faker->sentence();

    // 随机取一个月以内的时间
    $updated_at = $faker->dateTimeThisMonth();

    // 传参为生成最大时间不超过，因为创建时间需永远比更改时间要早
    $created_at = $faker->dateTimeThisMonth($updated_at);
    $title = $faker->words(1, true);
    return [
        'title' => $title,
        'description' => $sentence,
        'slug' => pinyin_permalink($title),
        'created_at' => $created_at,
        'updated_at' => $updated_at,
    ];
});
