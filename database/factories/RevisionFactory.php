<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WikiRevision;
use Faker\Generator as Faker;

$factory->define(WikiRevision::class, function (Faker $faker) {
    return [
        'changelog' => $faker->sentence,
        'type' => 'version'
    ];
});
