<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id')->nullable();
            $table->string('title');
            $table->string('slug')->nullable()->index();
            $table->string('icon')->nullable();
            $table->string('banner')->nullable();
            $table->string('description')->nullable();
            $table->json('settings')->nullable(); // title_color/description_color
            $table->json('cache')->nullable(); // threads_count/views_count/followers_count
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
