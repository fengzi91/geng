<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWikiRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wiki_revisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('changelog')->nullable();
            $table->longText('body')->nullable();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('wiki_id')->index();
            $table->string('type')->default('version')->index();
            $table->json('cache')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wiki_revisions');
    }
}
