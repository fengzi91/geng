<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/{order?}', 'HomeController@index')->name('home');

Route::resource('/articles', 'ArticlesController');

Route::post('upload_image', 'ArticlesController@uploadImage')->name('articles.upload_image');

Route::resource('/comments', 'CommentsController');

Route::resource('/categories', 'CategoriesController');

Route::get('/wiki/{category}', 'CategoriesController@articles')->name('wiki.index');

Route::post('/wikis/{wiki}/set', 'WikisController@set')->name('wikis.set');

Route::resource('/wikis', 'WikisController', ['only' => ['index', 'create', 'store', 'show']]);

// 创建一个版本
Route::get('/wikis/{wiki}/create', 'WikiRevisionsController@create')
    ->name('wiki.revisions.create');

Route::post('/wikis/{wiki}', 'WikiRevisionsController@store')
    ->name('wiki.revisions.store');

Route::get('/wikis/{wiki}/edit/{revision}', 'WikiRevisionsController@edit')
    ->name('wiki.revisions.edit');

Route::put('/wikis/{wiki}/edit/{revision}', 'WikiRevisionsController@update')
    ->name('wiki.revisions.update');

Route::get('/wikis/{wiki}/{revision}', 'WikiRevisionsController@show')
    ->name('wiki.revisions.show');

Route::get('relations', 'RelationController@index')
    ->name('relations.index');

Route::post('relations/{relation}', 'RelationController@toggleRelation')
    ->name('relations.toggle');

Route::resource('/users', 'UsersController');

Route::get('/revisions/{revision}', 'WikiRevisionsController@show')->name('revisions.show');
