<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm py-1 shadow-6">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav flex-row align-items-center justify-content-center py-sm-2 py-md-0 mx-auto">

                <li class="nav-item active">
                    <a href="/" class="nav-link">首页</a>
                </li>
                <li class="nav-item">
                    <a href="/" class="nav-link">动态</a>
                </li>
                @include('layouts._category_item')
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-md-auto flex-row d-md-flex align-items-center justify-content-around">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a href="{{ route('wikis.create') }}"
                           class="text-20 btn btn-icon btn-transparent btn-light active">
                            <i class="mdi mdi-plus"
                               aria-hidden="true"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <div class="btn-group">
                            <a id="navbarDropdown" class="dropdown-toggle cursor-pointer" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="{{ Auth::user()->avatar }}" class="avatar-40 mr-2"/>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <div class="dropdown-item">
                                    <a href="/users/{{ Auth::user()->id }}" class="">
                                        <div class="text-16 text-gray-30">{{ Auth::user()->name }}</div>
                                        <div>{{ '@' . Auth::user()->name }}</div>
                                    </a>
                                </div>
                                <div class="dropdown-divider"></div>
                                <a href="/user/profile" class="dropdown-item">个人中心</a>
                                <a href="/user/profile" class="dropdown-item">编辑资料</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
