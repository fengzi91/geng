<!-- 如果当前类目有 children 字段并且 children 字段不为空 -->
@foreach($categoryTree as $category)
@if(isset($category['children']) && count($category['children']) > 0)
  <li class="nav-item">
    <div class="btn-group">
        <a href="#" id="categoryDropdown-{{ $category['id'] }}" class="dropdown-item dropdown-toggle" data-toggle="dropdown">{{ $category['title'] }}</a>
        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="categoryDropdown-{{ $category['id'] }}">
          <!-- 遍历当前类目的子类目，递归调用自己这个模板 -->
          <a href="{{ route('wiki.index', $category['slug']) }}" class="dropdown-item">{{ $category['title'] }}</a>
          @foreach($category['children'] as $childCategory)
            <a href="{{ route('wiki.index', $childCategory['slug']) }}" class="dropdown-item">{{ $childCategory['title'] }}</a>
          @endforeach
        </div>
    </div>
  </li>
@endif
@endforeach
