@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-threads-show">
            <div class="row mt-md-3">
                <div class="col-md-9 m-auto">
                    <wiki-create-edit inline-template>
                        <form method="post" :action="action">
                            @csrf
                            <input type="hidden" name="type" v-model="createType">
                            <div class="input-group">
                                <input name="title" class="form-control form-control-lg" type="text"
                                       disabled
                                       value="{{ $wiki->title }}"/>
                            </div>
                            <div class="editor mt-3">
                                <textarea name="body" class="form-control"
                                          id="editor">{{ old('body', $revision->content->body ?? '') }}</textarea>
                            </div>
                            @if ($errors->has('body'))
                                <div class="flash-message mt-2">
                                    <p class="alert alert-danger">
                                        {{ $errors->first('body') }}
                                    </p>
                                </div>
                            @endif
                            <div class="mt-md-3 mt-1">
                                <span class="text-muted">选择标签</span>
                                <div class="message" v-if="categoryMsg">
                                    <div class="alert alert-warning mb-0">@{{ categoryMsg }}</div>
                                </div>
                            </div>
                            <div class="mt-1 d-flex flex-wrap">
                                @if(count($categories) > 0)
                                    <template v-for="(category, index) in categories">
                                        <div>
                                            <div class="text-muted">@{{ category.title }}</div>
                                            <button
                                                style="margin: .25rem;"
                                                @click.stop.prevent="checkCategory(index, i, cate.id)"
                                                :class="{'btn': true, 'btn-primary': cate.check, 'btn-light': !cate.check}"
                                                v-for="(cate, i) in category.children"
                                            >@{{ cate.title }}
                                            </button>
                                        </div>

                                    </template>
                                @else
                                    <div class="m-1">还没有标签</div>
                                @endif
                            </div>
                            <div class="mt-1 d-flex flex-wrap">
                                <div class="mr-2" style="margin: 1px;">
                                    <button class="btn btn-secondary"
                                            @click.stop.prevent="showAddCategory">@{{ addCate ? '取消' : '添加标签' }}
                                    </button>
                                </div>
                                <div class="input-group w-75" v-if="addCate">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-secondary dropdown-toggle"
                                                type="button" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">@{{ addCategoryParentTitle }}
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="javascript:;"
                                               @click="selectAddCategoryParentId(category.id, category.title)"
                                               v-for="category in categories"
                                               v-if="category.category_id <= 0">
                                                @{{ category.title }}
                                            </a>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control"
                                           aria-label="Text input with dropdown button" v-model="addCategoryTitle"
                                           placeholder="输入标题">
                                    <div class="input-group-append">
                                        <button v-if="addCate" class="btn btn-secondary"
                                                @click.stop.prevent="addCategory">添加
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('category'))
                                <div class="flash-message mt-2">
                                    <p class="alert alert-danger">
                                        {{ $errors->first('category') }}
                                    </p>
                                </div>
                            @endif
                            <input type="hidden" name="category" :value="selectCategories">
                            <div class="mt-md-3 mt-1">
                                <input name="changelog" class="form-control form-control-lg" type="text"
                                       placeholder="输入修改理由"
                                       value="{{ old('changelog') }}"/>
                            </div>
                            <div class="mt-md-3 mt-1">
                                @if(Auth::user()->hasAnyRole(['Founder', 'Maintainer']))
                                <button type="submit" class="btn btn-primary">提交为新版本</button>
                                <input type="hidden" name="_method" value="put" v-if="isUpdate">
                                <button type="submit" class="btn btn-primary" @click="submitType('update')">合并到当前版本</button>
                                @else
                                    <button type="submit" class="btn btn-primary">提交</button>
                                @endif
                                <button type="submit" class="btn btn-secondary ml-1" @click="createType = 'draft'">
                                    保存为草稿
                                </button>
                            </div>
                        </form>
                    </wiki-create-edit>
                </div>
            </div>
        </div>
    </div>
@stop
@prepend('scripts')
    <script>
        var categories = @json($categories);
        var create_action = '{{ route('wiki.revisions.store', $wiki->id) }}';
        var update_action = '{{ route('wiki.revisions.update', ['wiki' => $wiki->id, 'revision' => $revision->id]) }}';
            @php
            if($revision && $revision->categories) {
                $s = $revision->categories->pluck('id')->toArray();
            } else {
                $s = explode(',', old('category'));
            }
                foreach($s as $k=>$v) {
                    $s[$k] = (integer) $v;
                }
            @endphp
        var selectCategories = @json($s);
    </script>
@endprepend
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/simditor.css') }}">
@stop
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/module.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/uploader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/simditor.js') }}"></script>
    <script>
        console.log(categories);
        $(document).ready(function () {
            var editor = new Simditor({
                textarea: $('#editor'),
                toolbar: [
                    'title',
                    'bold',
                    'italic',
                    'underline',
                    'strikethrough',
                    'fontScale',
                    'color',
                    'ol',
                    'ul',
                    'blockquote',
                    'table',
                    'link',
                    'image',
                    'hr',
                    'indent',
                    'outdent',
                    'alignment'
                ],
                upload: {
                    url: '{{ route('articles.upload_image') }}',
                    params: {
                        _token: '{{ csrf_token() }}'
                    },
                    fileKey: 'upload_file',
                    connectionCount: 3,
                    leaveConfirm: '文件上传中，关闭此页面将取消上传。'
                },
                pasteImage: true,
            });
        });
    </script>
@stop
