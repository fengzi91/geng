@if (count($comments) > 0)
    @foreach($comments as $comment)
        <div id="comment-{{ $comment->id }}" name="comment-{{ $comment->id }}" class="border-bottom box-body py-2">
            <div class="user-media d-flex">
                <a href="#" class="">
                    <img src="{{ $comment->user->avatar }}" alt="{{ $comment->user->name }}" class="avatar-40">
                </a>
                <div class="ml-2">
                    <div><a href="#" class="">
                            <h6 class="mb-0 text-16 d-inline-block text-black-50 text-14">{{ $comment->user->name }}</h6>
                            <a href="#" class="text-muted text-12 ml-1">{{ $comment->user->name }}</a></a>
                    </div>
                    <small><a href="#comment-{{ $comment->id }}"
                              class="text-gray-70">{{ $comment->created_at->diffForHumans() }}</a></small>
                </div>
                <div class="text-16 text-gray-60 ml-auto d-flex align-items-center">
                    <div class="mx-1 cursor-pointer d-flex">
                        <button class="btn btn-icon btn-light text-gray-60">
                            <span class="mdi mdi-thumb-up"></span>
                        </button>
                        <span class="ml-1 align-self-center">0</span>
                    </div>
                </div>
            </div>
            <section class="comment-content text-gray-40 pt-2">
                <p>{!! $comment->content->body !!}</p>
            </section>
        </div>
    @endforeach
    {!! $comments->render() !!}
    @else
    @include('common._no_data', ['show' => '还没有人评论~'])
@endif
