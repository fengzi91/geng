<div class="box mb-3">
    @auth
    <form method="POST" action="{{ route('comments.store') }}#comments">
        @csrf
        <input type="hidden" name="commentable_type" value="{{ get_class($wiki) }}"/>
        <input type="hidden" name="commentable_id" value="{{ $wiki->id }}"/>
        <div class="d-flex align-items-center w-100">
            <img src="{{ Auth::user()->avatar }}" class="avatar-40"/>
            <div class="text-18 text-muted ml-2 w-100">
                                            <textarea class="form-control" name="content"
                                                      placeholder="撰写评论..."></textarea>
            </div>
        </div>
        <div class="d-flex justify-content-between mt-2">
            @if ($errors->has('content'))
                <div class="flash-message ml-4">
                    <p class="alert alert-danger mb-0 ml-1 p-1">
                        {{ $errors->first('content') }}
                    </p>
                </div>
            @else
                <div class="flash-message ml-4">
                    <p class="alert alert-info mb-0 ml-1 p-1">
                        请不要发表不友好的评论
                    </p>
                </div>
            @endif
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </form>
    @endauth
    @guest
        <a href="{{ route('login') }}">登录</a> 后可发表评论
    @endguest
</div>
