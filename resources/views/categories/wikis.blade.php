@extends('layouts.app')

@section('content')
    <header class="page-header bg-grey-blue py-3 text-white">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1>{{ $category->title }}</h1>
                    <p>{{ $category->description }}</p>
                    @if(count($category->children) > 0)
                        <div>相关话题</div>
                        @foreach($category->children as $childrenCategory)
                            <a class="text-white mr-2"
                               href="{{ route('wiki.index', $childrenCategory->slug) }}">#{{ $childrenCategory->title }}</a>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-6 d-flex justify-content-end">

                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row mt-3">
            <div class="col-md-9">
                <div class="box box-flush">
                    <div class="box-body">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a href="{{ url()->current() }}" class="nav-link {{ active_class(if_query('order', '')) }}">最新发布</a></li>
                            <li class="nav-item"><a href="?order=active" class="nav-link {{ active_class(if_query('order', 'active')) }}">活跃</a></li>
                            <li class="nav-item"><a href="?order=excellent" class="nav-link {{ active_class(if_query('order', 'excellent')) }}">精选</a></li>
                            <li class="nav-item"><a href="?order=no_reply" class="nav-link {{ active_class(if_query('order', 'no_reply')) }}">零回复</a></li>
                        </ul>
                    </div>
                    <div class="threads-items mb-2">
                        @if(count($wikis) > 0)
                            @include('wikis._wikis_list', ['wikis' => $wikis])
                        @else
                            @include('common._no_data')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
