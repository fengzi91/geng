@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-threads-show">
            <div class="row mt-md-3">
                <div class="col-md-9 m-auto">
                    <form method="post" action="{{ route('wikis.store') }}">
                            @csrf
                            <div class="input-group">
                                <input name="title" class="form-control form-control-lg" type="text"
                                       placeholder="输入名称"
                                       value="{{ old('title', '') }}"/>
                            </div>
                            @if ($errors->has('title'))
                                <div class="flash-message mt-2">
                                    <p class="alert alert-danger">
                                        {{ $errors->first('title') }}
                                    </p>
                                </div>
                            @endif
                            <div class="mt-md-3 mt-1">
                                <button type="submit" class="btn btn-primary">提交</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@stop
