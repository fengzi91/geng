@if(count($wiki_logs) > 0)
<div class="box my-3">
    <ul class="list-group list-group-flush">
        @foreach($wiki_logs as $log)
            <li class="list-group-item d-md-flex d-block align-items-center justify-content-between">
                <div class="d-flex align-items-center w-70">
                    <a href="#" class="mr-2">
                        <img src="{{ $log->causer->avatar }}" alt="{{ $log->causer->name }}" class="avatar-30" />
                    </a>
                    @if($log->log_name === 'wiki.created')

                    <div class="text-gray-50">
                        {{ $log->description }} {{ $log->subject->title }}
                    </div>
                    @elseif($log->log_name === 'wiki.revision.created')
                    <div class="text-gray-50">
                        {{ $log->description }} <a href="{{ route('wiki.revisions.show', ['wiki' => $wiki->id, 'revision' => $log->getExtraProperty('revision')['id']]) }}">
                            {{ $log->getExtraProperty('revision')['changelog'] ?? $log->subject->title }}</a>
                    </div>
                    @elseif($log->log_name === 'wiki.categories.change')
                        <div class="text-gray-50">
                            {{ $log->description }}
                            @foreach($log->getExtraProperty('categories') as $category)
                            <a href="{{ route('wiki.index', $category['slug']) }}">{{ $category['title'] }}</a>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="ml-auto d-flex align-items-center justify-content-md-end">
                    <div class="ml-1 text-gray-60">
                        <small>{{ $log->created_at->diffForHumans() }}</small>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>
@endif
