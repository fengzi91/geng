@extends('layouts.app')


@section('content')
    <div class="container py-3">
        <div class="page-threads-show pb-4">
            <div class="row">
                <div class="col-md-9">
                    <article class="box box-flush">
                        <div class="thread-content box-body text-gray-40 text-16">
                            <header>
                                <h2 class="mb-3 pb-2 border-bottom">
                                    {{ $wiki->title }}
                                </h2>
                            </header>
                            <section>
                                @if($revision)
                                    {!! $revision->content->body ?? '' !!}
                                @else
                                    @include('common._no_data')
                                    <a href="{{ route('wiki.revisions.create', $wiki->id) }}" class="btn btn-primary">我来完善</a>
                                @endif
                            </section>
                        </div>
                        @if($revision && count($revision->categories) > 0)
                            <div class="thread-stats-bar bg-white border-top py-1">
                                <div class="container">
                                    @foreach($revision->categories as $category)
                                        <a href="{{ route('wiki.index', $category->slug) }}"
                                           class="badge badge-primary">#{{ $category->title }}</a>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="thread-stats-bar bg-white border-top py-1">
                            <div class="container">
                                <ul class="nav align-items-center">
                                    <li class="nav-item">
                                        <div>
                                            <a href="javascript:;"
                                               onclick="event.preventDefault();document.getElementById('wiki-up-{{ $wiki->id }}').submit();"
                                               class="btn btn-sm btn-link {{ $wiki->isLikedBy(Auth::user()) ? 'text-pink' : 'text-gray-50'}}"
                                               data-toggle="tooltip"
                                               data-placement="auto"
                                               title="{{ $wiki->isLikedBy(Auth::user()) ? '取消赞' : $wiki->cache['likes_count'] . '人点赞' }} ">
                                                <span class="mdi mdi-thumb-up"></span>
                                                {{ $wiki->cache['likes_count'] }}
                                            </a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <div>
                                            <a href="#"
                                               class="btn btn-sm btn-link text-gray-50"
                                               data-toggle="tooltip"
                                               data-placement="auto" title="{{ $wiki->cache['comments_count'] }} 条评论">
                                                <span class="mdi mdi-comment"></span>
                                                {{ $wiki->cache['comments_count'] }}
                                            </a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <div>
                                            <a href="#"
                                               class="btn btn-sm btn-link text-gray-50"
                                               data-toggle="tooltip"
                                               data-placement="auto" title="{{ $wiki->cache['views_count'] }} 次查看">
                                                <span class="mdi mdi-eye"></span>
                                                {{ $wiki->cache['views_count'] }}
                                            </a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <div>
                                            <a href="#" class="btn btn-sm btn-link text-gray-50">
                                                <span class="mdi mdi-dots-vertical"></span>
                                            </a>
                                        </div>
                                    </li>
                                <!--li class="nav-item ml-auto">
                                        <div>
                                            <a href="{{ route('wiki.revisions.create', $wiki->id) }}"
                                               class="btn btn-sm btn-link text-gray-50"
                                               data-toggle="tooltip" data-placement="auto" title="新建一个版本"
                                            >
                                                <span class="mdi mdi-plus"></span>
                                            </a>
                                        </div>
                                    </li-->
                                    @if($revision && ((Auth::user() && Auth::user()->hasAnyRole(['Founder', 'Administrator', 'Editor']))  || $revision->is_default_version))
                                        <li class="nav-item ml-auto">
                                            <div>
                                                <a href="{{ route('wiki.revisions.edit', ['wiki' => $wiki->id, 'revision' => $revision->id]) }}"
                                                   class="btn btn-sm btn-link text-gray-50"
                                                   data-toggle="tooltip" data-placement="auto" title="编辑"
                                                >
                                                    <span class="mdi mdi-file-edit"></span>
                                                </a>
                                            </div>
                                        </li>
                                    @endif
                                    @auth
                                        @if(Auth::user()->hasAnyRole(['Founder', 'Maintainer']))
                                            <li class="nav-item">
                                                <div>
                                                    <a href="javascript:;"
                                                       onclick="event.preventDefault();document.getElementById('article-set-pinned').submit();"
                                                       class="btn btn-sm btn-link {{ $wiki->pinned_at ? 'text-pink' : 'text-gray-50' }}"
                                                       data-toggle="tooltip"
                                                       data-placement="auto"
                                                       title="{{ $wiki->pinned_at ? '取消' : ''}}置顶">
                                                        <span class="mdi mdi-arrow-up"></span>
                                                    </a>
                                                    <form id="article-set-pinned"
                                                          action="{{ route('wikis.set', $wiki->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="type" value="pinned"/>
                                                    </form>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <div>
                                                    <a href="javascript:;"
                                                       onclick="event.preventDefault();document.getElementById('article-set-excellent').submit();"
                                                       class="btn btn-sm btn-link {{ $wiki->excellent_at ? 'text-pink' : 'text-gray-50' }}"
                                                       data-toggle="tooltip"
                                                       data-placement="auto"
                                                       title="{{ $wiki->excellent_at ? '取消' : ''}}精华">
                                                        <span class="mdi mdi-diamond-stone"></span>
                                                    </a>
                                                    <form id="article-set-excellent"
                                                          action="{{ route('wikis.set', $wiki->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="type" value="excellent"/>
                                                    </form>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <div>
                                                    <a href="javascript:;"
                                                       onclick="event.preventDefault();document.getElementById('article-set-frozen').submit();"
                                                       class="btn btn-sm btn-link {{ $wiki->frozen_at ? 'text-pink' : 'text-gray-50'  }}"
                                                       data-toggle="tooltip"
                                                       data-placement="auto"
                                                       title="{{ $wiki->frozen_at ? '取消' : ''}}锁定">
                                                    <span
                                                        class="mdi {{ $wiki->frozen_at ? 'mdi-lock' : 'mdi-lock-open'}}"></span>
                                                    </a>
                                                    <form id="article-set-frozen"
                                                          action="{{ route('wikis.set', $wiki->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="type" value="frozen"/>
                                                    </form>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <div>
                                                    <a href="javascript:;"
                                                       onclick="event.preventDefault();document.getElementById('article-set-banned').submit();"
                                                       class="btn btn-sm btn-link {{ $wiki->banned_at ? 'text-pink' : 'text-gray-50' }}"
                                                       data-toggle="tooltip"
                                                       data-placement="auto"
                                                       title="{{ $wiki->banned_at ? '取消' : ''}}封禁">
                                                        <span class="mdi mdi-alert-box"></span>
                                                    </a>
                                                    <form id="article-set-banned"
                                                          action="{{ route('wikis.set', $wiki->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="type" value="banned"/>
                                                    </form>
                                                </div>
                                            </li>
                                        @endif
                                    @endauth
                                </ul>
                            </div>
                        </div>
                        <div class="thread-author-card border-top p-3">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="user-info d-flex align-items-center">
                                    <a href="#">
                                        <img src="{{ $wiki->founder->avatar }}" alt="{{ $wiki->founder->name }}"
                                             class="avatar-60"/>
                                    </a>
                                    <div class="p-2">
                                        <a href="#">
                                            <h3 class="text-gray-50 text-14">{{ $wiki->founder->name }}</h3>
                                        </a>
                                        <div class="text-12 text-muted">用户介绍~~~~~</div>
                                    </div>
                                </div>
                                <div class="right-action">
                                    <a href="javascript:;" onclick="event.preventDefault();document.getElementById('follow-user-{{ $wiki->founder->id }}').submit();" class="btn btn-rounded btn-outline-teal-blue mx-1">
                                        <span class="mdi mdi-plus"></span>
                                        关注 Ta
                                    </a>
                                </div>
                                <form id="follow-user-{{ $wiki->founder->id }}"
                                     action="{{ route('relations.toggle', 'follow') }}" method="POST" style="display: none;">
                                     @csrf
                                     <input type="hidden" name="followable_type" value="App\\User"/>
                                    <input type="hidden" name="followable_id" value="{{ $wiki->founder->id }}"/>
                                </form>
                            </div>
                        </div>
                    </article>
                    @include('wikis._logs', compact('wiki_logs'))
                    <div class="thread-toolbar">
                        <div class="text-center clap d-flex align-items-center justify-content-center cursor-pointer">
                            <a href="javascript:;"
                               onclick="event.preventDefault();document.getElementById('wiki-up-{{ $wiki->id }}').submit();"
                               class="btn btn-icon clap">
                                <span
                                    class="mdi {{ $wiki->isLikedBy(Auth::user()) ? 'mdi-thumb-up' : 'mdi-thumb-up-outline'}}"
                                    style="color: #0078ff;"></span>
                            </a>
                            <form id="wiki-up-{{ $wiki->id }}" action="{{ route('relations.toggle', 'like') }}"
                                  method="POST">
                                @csrf
                                <input type="hidden" name="followable_type" value="App\\Wiki"/>
                                <input type="hidden" name="followable_id" value="{{ $wiki->id }}"/>
                            </form>
                        </div>
                        @if($revision && (Auth::user() && Auth::user()->hasAnyRole(['Founder', 'Administrator', 'Editor']) || ($revision->is_default_version)))
                            <div
                                class="text-center clap d-flex align-items-center justify-content-center cursor-pointer mt-1">
                                <a href="{{ route('wiki.revisions.edit', ['wiki' => $wiki->id, 'revision' => $revision->id]) }}"
                                   class="btn btn-icon clap">
                                    <span class="mdi mdi-file-document-edit-outline" style="color: #0078ff;"></span>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="thread-comments mt-3">
                        <div class="comments" name="comments">
                            <div class="py-2 text-16 text-gray-50">
                                {{ $wiki->cache['comments_count'] }} 条评论
                            </div>
                            @include('comments._create_form')
                            <div class="box box-flush">
                                <div class="border-bottom box-body py-2">
                                    @include('comments._comment', ['comments' => $comments])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    @if($revision && count($wiki->revisions) > 0)
                        <div class="box text-gray-50">
                            <div class="box-heading d-flex align-items-center justify-content-between text-13">
                                版本记录
                            </div>
                            <nav class="nav flex-column">
                                @foreach($wiki->revisions as $version)
                                    <li class="py-1 d-flex justify-content-around">
                                        <a href="{{ route('wiki.revisions.show', ['wiki' => $wiki->id, 'revision' => $version->id]) }}"
                                           class="flex-grow-1 {{ active_class(if_route_param('version', $version->id), 'text-primary', 'text-gray-50') }}"
                                        >
                                            <span class="mdi mdi-file-document"></span>
                                            {{ $version->changelog ?? '#' . ($loop->remaining + 1) }}

                                        </a><span class="text-muted">{{ $version->created_at->diffForHumans() }}</span>
                                    </li>
                                @endforeach
                            </nav>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
