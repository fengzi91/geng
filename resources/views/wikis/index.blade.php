@extends('layouts.app')

@section('content')
    <div class="container my-3">
        <div class="row">
            <div class="col-md-9">
                <div class="box box-flush">
                    <div class="box-body">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a href="/?hot" class="nav-link active">活跃</a></li>
                            <li class="nav-item"><a href="/?j" class="nav-link">精选</a></li>
                            <li class="nav-item"><a href="/?no_reply" class="nav-link">零回复</a></li>
                            <li class="nav-item"><a href="/?new" class="nav-link">最新发布</a></li>
                        </ul>

                    </div>
                    <div class="threads-items mb-2">
                        @include('wikis._wikis_list')
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
@stop
