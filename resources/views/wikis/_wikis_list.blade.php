@if(count($wikis) > 0)
<ul class="list-group list-group-flush">
    @foreach($wikis as $wiki)
        <li class="list-group-item d-md-flex d-block align-items-center justify-content-between cursor-pointer">
            <div class="d-flex align-items-center w-70">
                <a href="#" class="mr-2">
                    <img src="{{ $wiki->founder->avatar }}" alt="{{ $wiki->founder->name }}" class="avatar-30">
                </a>
                <div class="text-gray-50">
                    @if ($wiki->pinned_at)
                        <span class="badge badge-danger">置顶</span>
                    @endif
                    @if($wiki->excellent_at)
                        <span class="badge badge-success">精华</span>
                    @endif
                    @if($wiki->frozen_at)
                        <span class="badge badge-secondary">锁定</span>
                    @endif
                    @if($wiki->banned_at)
                        <span class="badge badge-secondary">封禁</span>
                    @endif
                    <a href="{{ route('wikis.show', $wiki->id) }}">{{ $wiki->title }}</a>
                </div>
            </div>
            <div class="ml-auto d-flex align-items-center justify-content-md-end">
                <div class="text-gray-60 d-flex justify-content-between align-items-center">
                                        <span class="p-1">
                                            <i class="mdi mdi-heart" aria-hidden="true"></i> {{ $wiki->cache['likes_count'] }}
                                        </span>
                    <span class="p-1">
                                            <i class="mdi mdi-comment" aria-hidden="true"></i> {{ $wiki->cache['comments_count'] }}
                                        </span>
                    <span class="p-1">
                                            <i class="mdi mdi-eye" aria-hidden="true"></i> {{ $wiki->cache['views_count'] }}
                                        </span>
                </div>
                <div class="ml-1 text-gray-60">
                    <small>{{ $wiki->updated_at->diffForHumans() }}</small>
                </div>
            </div>

        </li>
    @endforeach
</ul>
{{ $wikis->appends(Request::except('page'))->render() }}
@else
    @include('common._no_data')
@endif
