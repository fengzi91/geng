@if ($paginator->hasPages())
    <ul class="paginator d-flex justify-content-center align-items-center">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())

        @else
            <li class="paginator-item">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><span class="mdi mdi-arrow-left"></span></a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="paginator-item disabled" aria-disabled="true"><a href="javascript:;">{{ $element }}</a></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="paginator-item active" aria-current="page"><a href="javascript:;">{{ $page }}</a></li>
                        @else
                        <li class="paginator-item"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="paginator-item">
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                    <span class="mdi mdi-arrow-right"></span>
                </a>
            </li>
        @else

        @endif
    </ul>
@endif
