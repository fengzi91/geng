@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
    <div class="flash-message">
        <p class="alert alert-danger">
            {{ $error }}
        </p>
    </div>
    @endforeach
@endif
