@extends('layouts.app')

@section('content')
    <div class="container py-3">
        <div class="row">
            <div class="col-md-9">
                <div class="box box-flush">
                    <div class="box-body">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a href="/" class="nav-link {{ active_class(if_route_param('order', '')) }}">最新发布</a></li>
                            <li class="nav-item"><a href="/active" class="nav-link {{ active_class(if_route_param('order', 'active')) }}">活跃</a></li>
                            <li class="nav-item"><a href="/excellent" class="nav-link {{ active_class(if_route_param('order', 'excellent')) }}">精选</a></li>
                            <li class="nav-item"><a href="/no_reply" class="nav-link {{ active_class(if_route_param('order', 'no_reply')) }}">零回复</a></li>
                        </ul>

                    </div>
                    <div class="threads-items mb-2">
                        @include('wikis._wikis_list', ['wikis' => $wikis])
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
@endsection
