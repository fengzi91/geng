const ERR_CODE_LIST = { //常见错误码列表
  [400]: "请求错误",
  // [401]: "登录失效或在其他地方已登录",
  [403]: "拒绝访问",
  [404]: "请求地址出错",
  [408]: "请求超时",
  [429]: "您的点击速度太快了，请稍候再试",
  [500]: "服务器内部错误",
  [501]: "服务未实现",
  [502]: "网关错误",
  [503]: "服务不可用",
  [504]: "网关超时",
  [505]: "HTTP版本不受支持"
}

export function getErrorMessage(error) {
    if (!error.response) {//无网络时单独处理
        return '网络错误，请稍候重试！'
    }
    const errorCode = error.response.status;
    let errorMessage = '';

    if (error.response.data.errors) {
        let keys = Object.keys(error.response.data.errors)
        errorMessage = error.response.data.errors[keys[0]][0]
    } else {
        errorMessage = ERR_CODE_LIST[errorCode];
    }

    return errorMessage;
}
