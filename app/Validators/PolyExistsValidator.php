<?php

namespace App\Validators;

use Arr;

class PolyExistsValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        if (!$objectType = Arr::get($validator->getData(), $parameters[0], false)) {
            return false;
        }
        try {
            return !empty(resolve(str_replace('\\\\', '\\', $objectType))->find($value));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());

            return false;
        }
    }
}
