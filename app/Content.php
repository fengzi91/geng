<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Mews\Purifier\Facades\Purifier;
use Str;
class Content extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'contentable_type', 'contentable_id', 'body',
    ];

    protected $casts = [
        'id' => 'int',
        'contentable_id' => 'int',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($content) {
            $content->body = Purifier::clean($content->body);
        });
    }

    public function contentable()
    {
        return $this->morphTo();
    }

    public function mentions()
    {
        return $this->belongsToMany(User::class, 'content_mention');
    }

    public function getActivityLogContentAttribute()
    {
        return Str::limit(strip_tags($this->body), 200);
    }
}
