<?php

namespace App\Observers;

use App\Comment;
use App\Article;

class CommentObserver
{
    public function saved(Comment $comment)
    {
        $comment->user->refreshCache();

        $this->createActionLog($comment);

        if (\is_callable([$comment->commentable, 'refreshCache'])) {
            $comment->commentable->refreshCache();
        }

        if ($comment->commentable instanceof Article) {
            // $comment->commentable->node->refreshCache();
            $comment->commentable->update([
                'activated_at' => now()
            ]);
        }

    }

    protected function createActionLog($comment)
    {
        if (!$comment->wasRecentlyCreated) {
            return;
        }

        $targetType = strtolower(class_basename($comment->commentable_type));

        activity('commented.'.$targetType)
            ->performedOn($comment->commentable)
            ->withProperty('content', $comment->content->activity_log_content)
            ->withProperty('comment_id', $comment->id)
            ->log('评论');
    }
}
