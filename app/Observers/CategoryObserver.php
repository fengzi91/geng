<?php

namespace App\Observers;
use App\Category;
class CategoryObserver
{

    public function saved(Category $category)
    {
        if ( ! $category->slug) {
            $slug = pinyin_permalink($category->title);
            // 判断是否已存在
            if (Category::where('slug', $slug)->first()) {
                $slug .= '-' . $category->id;
            }
            \DB::table('categories')->where('id', $category->id)->update(
                ['slug' => $slug]
            );
        }
    }

}
