<?php

namespace App\Observers;

use App\WikiRevision;
use App\Wiki;
class WikiRevisionObserver
{

    public function saved(WikiRevision $revision)
    {
        $wiki = $revision->wiki;
        $version = $wiki->version;
        if ($version->id === $revision->id) {
            // 更新主文档的关联
            $categories = $version->categories->pluck('id')->toArray();
            $wiki->categories()->sync($categories);
        }
    }
}
