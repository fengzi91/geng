<?php

namespace App;

use App\Contracts\Commentable;
use Overtrue\LaravelFollow\Traits\CanBeFavorited;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Overtrue\LaravelFollow\Traits\CanBeSubscribed;
use Illuminate\Database\Eloquent\SoftDeletes;
class Article extends Model implements Commentable
{
    use SoftDeletes, CanBeSubscribed, CanBeFavorited, CanBeLiked;
    protected $fillable = ['title', 'founder_id', 'excellent_at',
        'pinned_at', 'frozen_at', 'banned_at', 'published_at', 'activated_at', 'cache',
        'cache->views_count', 'cache->comments_count', 'cache->likes_count', 'cache->subscriptions_count',
        'cache->last_reply_user_id', 'cache->last_reply_user_name', ];

    protected $with = [
        'founder',
        'categories',
    ];

    protected $casts = [
        'id' => 'int',
        'user_id' => 'int',
        'is_excellent' => 'bool',
        'cache' => 'array',
    ];

    protected $dates = [
        'excellent_at', 'pinned_at', 'frozen_at', 'banned_at', 'published_at',
    ];

    const CACHE_FIELDS = [
        'views_count' => 0,
        'comments_count' => 0,
        'likes_count' => 0,
        'subscriptions_count' => 0,
        'last_reply_user_id' => 0,
        'last_reply_user_name' => null,
    ];

    const STATUS_FIELDS = [
        'excellent' => '精华',
        'pinned' => '置顶',
        'frozen' => '锁定',
        'banned' => '封禁',
    ];

    // 创建者
    public function founder()
    {
        return $this->belongsTo(User::class);
    }

    // 参与编辑者
    public function user()
    {

    }

    public function scopePublished($query)
    {

    }

    public function scopeActivity($query)
    {
        $query->orderBy('banned_at', 'asc')
            ->orderBy('pinned_at', 'desc')
            ->orderBy('activated_at', 'desc')
            ->where('article_id', 0);
    }

    public function content()
    {
        return $this->morphOne(Content::class, 'contentable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    // 关联分类 / 标签
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'article_category');
    }
    public function afterCommentCreated(Comment $lastComment)
    {
        // $lastComment->user->subscribe($this);
    }

    // 更新统计字段
    public function refreshCache()
    {
        $lastComment = $this->comments()->latest()->first();

        $this->update(['cache' => \array_merge(self::CACHE_FIELDS, [
            'views_count' => $this->cache['views_count'],
            'comments_count' => $this->comments()->count(),
            'likes_count' => $this->likers()->count(),
            'subscriptions_count' => $this->subscribers()->count(),
            'last_reply_user_id' => $lastComment ? $lastComment->user->id : 0,
            'last_reply_user_name' => $lastComment ? $lastComment->user->name : '',
        ])]);
    }

    public function activate()
    {
        return $this->update(['activated_at' => now()]);
    }

    public function article()
    {
        return $this->hasMany(self::class);
    }

    public function version($version = null)
    {
        if ($version) return $this->find($version);
        else {
            return $this->where('article_id', $this->id)
                ->orWhere('article_id', 0)->latest()->first();
        }
    }
    public function versions()
    {
        return $this->hasMany(self::class);
    }
}
