<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        switch ($this->method()) {
            case 'POST':
                $rules['title'] = [
                    'required',
                    'between:1,120',
                    //'unique:articles'
                ];
            case 'PUT':
                $rules['title'] = [
                    'required',
                    'between:1,120',
                    //'unique:articles,title,' . request()->id,
                ];
            default:

                $rules['body'] = [
                    'required',
                    'min:10',
                ];
                $rules['category'] = [
                    'required'
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => '请填写标题',
            'title:unique' => '标题已存在',
            'body.required' => '请填写正文',
            'category.required' => '请至少选择一个标签',
        ];
    }
}
