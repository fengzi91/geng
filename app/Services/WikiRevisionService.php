<?php

namespace App\Services;

use App\Category;
use App\Wiki;
use App\WikiRevision;

class WikiRevisionService
{

    public function createEmptyRevision(Wiki $wiki)
    {
        $wiki->revisions()->create([

        ]);
    }

    public function storeRevision()
    {

    }

    public function checkChangeCategory()
    {

    }

    public function checkChangeBody()
    {

    }
}
