<?php

namespace App;

use App\Contracts\Commentable;
use Overtrue\LaravelFollow\Traits\CanBeFavorited;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Overtrue\LaravelFollow\Traits\CanBeSubscribed;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wiki extends Model implements Commentable
{
    use SoftDeletes, CanBeSubscribed, CanBeFavorited, CanBeLiked;
    protected $fillable = ['title', 'founder_id', 'excellent_at',
        'pinned_at', 'frozen_at', 'banned_at', 'published_at', 'activated_at', 'cache',
        'cache->views_count', 'cache->comments_count', 'cache->likes_count', 'cache->subscriptions_count',
        'cache->last_reply_user_id', 'cache->last_reply_user_name', 'cache->last_edit_user_name', 'cache->last_edit_user_id'];

    protected $with = [
        'founder',
    ];

    protected $casts = [
        'id' => 'int',
        'user_id' => 'int',
        'is_excellent' => 'bool',
        'cache' => 'array',
    ];

    protected $dates = [
        'excellent_at', 'pinned_at', 'frozen_at', 'banned_at', 'published_at',
    ];

    const CACHE_FIELDS = [
        'views_count' => 0,
        'comments_count' => 0,
        'likes_count' => 0,
        'subscriptions_count' => 0,
        'last_reply_user_id' => 0,
        'last_reply_user_name' => null,
        'last_edit_user_name' => null,
        'last_edit_user_id' => 0,
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_wiki');
    }

    public function getVersionAttribute()
    {
        return $this->revisions()->first();
    }

    public function scopeRecent($query)
    {
        return $query->whereNull('banned_at')->with('founder')->orderBy('pinned_at', 'desc')->orderBy('excellent_at', 'desc')->orderBy('created_at', 'desc');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function founder()
    {
        return $this->belongsTo(User::class);
    }

    // 关联分类 / 标签


    public function afterCommentCreated(Comment $lastComment)
    {
        // $lastComment->user->subscribe($this);
    }

    public function revisions()
    {
        return $this->hasMany(WikiRevision::class)->where('type', '=', 'version')->orderBy('created_at', 'desc')->orderBy('id', 'desc');
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'uploaded_to')->orderBy('order', 'asc');
    }

    public function getCurrentRevision()
    {
        return $this->revisions()->first();
    }

    // 更新统计字段
    public function refreshCache()
    {
        $lastComment = $this->comments()->latest()->first();

        $this->update(['cache' => \array_merge(self::CACHE_FIELDS, [
            'views_count' => $this->cache['views_count'],
            'comments_count' => $this->comments()->count(),
            'likes_count' => $this->likers()->count(),
            'subscriptions_count' => $this->subscribers()->count(),
            'last_reply_user_id' => $lastComment ? $lastComment->user->id : 0,
            'last_reply_user_name' => $lastComment ? $lastComment->user->name : '',
        ])]);
    }


}
