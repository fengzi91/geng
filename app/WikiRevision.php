<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WikiRevision extends Model
{
    protected $fillable = ['type', 'wiki_id', 'title', 'changelog', 'user_id'];

    const CACHE_FIELDS = [
        'views_count' => 0,
    ];

    // 关联分类 / 标签
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_wiki_revision');
    }

    public function content()
    {
        return $this->morphOne(Content::class, 'contentable');
    }

    public function wiki()
    {
        return $this->belongsTo(Wiki::class);
    }

}
