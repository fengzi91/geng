<?php

namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    public function scopeRecent($query)
    {
        return $query->orderBy('id', 'desc');
    }
    public function scopeOrdered($query, $order)
    {
        switch ($order) {
            case 'active':
                return $query->orderBy('cache->comments_count', 'desc')
                    ->orderBy('pinned_at', 'desc')
                    ->orderBy('excellent_at', 'desc');
                break;
            case 'no_reply':
                return $query->where('cache->comments_count', 0)
                    ->orderBy('pinned_at', 'desc')
                    ->orderBy('excellent_at', 'desc');
                break;
            case 'excellent':
                return $query->orderBy('pinned_at', 'desc')
                    ->orderBy('excellent_at', 'desc');
                break;
            default:
               return $query->orderBy('pinned_at', 'desc')
                   ->orderBy('updated_at', 'desc')
                   ->orderBy('created_at', 'desc');
                break;
        }
    }
}
