<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use App\Validators\PolyExistsValidator;
class AppServiceProvider extends ServiceProvider
{
    protected $validators = [
        'poly_exists' => PolyExistsValidator::class,
        /*
         *
        'phone' => PhoneValidator::class,
        'id_no' => IdNumberValidator::class,
        'verify_code' => PhoneVerifyCodeValidator::class,
        'keep_word' => KeepWordValidator::class,
        'hash' => HashValidator::class,
        'ticket' => TicketValidator::class,
        'username' => UsernameValidator::class,
        'user_unique_content' => UserUniqueContentValidator::class,
        */
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (app()->isLocal()) {
            $this->app->register(\VIACreative\SudoSu\ServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::defaultView('vendor.pagination.geng');

        Paginator::defaultSimpleView('vendor.pagination.geng');

        \App\Wiki::observe(\App\Observers\WikiObserver::class);
        \App\WikiRevision::observe(\App\Observers\WikiRevisionObserver::class);
        \App\Category::observe(\App\Observers\CategoryObserver::class);
        \App\Comment::observe(\App\Observers\CommentObserver::class);
        $this->registerValidators();
        // 渲染指定模板 加入分类
        \View::composer(['*'], \App\Http\ViewComposers\CategoryTreeComposer::class);
    }

    protected function registerValidators()
    {
        foreach ($this->validators as $rule => $validator) {
            Validator::extend($rule, "{$validator}@validate");
        }
    }
}
