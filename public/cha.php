<?php
// 云市场分配的密钥Id
$secretId = 'AKIDEYuBwoL936xd77TZ0E22amUdlSVMr543GHz2';
// 云市场分配的密钥Key
$secretKey = 'h4ZuupR3B17tVnXS1Raq7n1xuG05o3lSoydhl6DY';
$source = 'market';

// 签名
$datetime = gmdate('D, d M Y H:i:s T');
$signStr = sprintf("x-date: %s\nx-source: %s", $datetime, $source);
$sign = base64_encode(hash_hmac('sha1', $signStr, $secretKey, true));
$auth = sprintf('hmac id="%s", algorithm="hmac-sha1", headers="x-date x-source", signature="%s"', $secretId, $sign);

// 请求方法
$method = 'GET';
// 请求头
$headers = array(
    'X-Source' => $source,
    'X-Date' => $datetime,
    'Authorization' => $auth,
);

$cardNum = $_GET['no'];
// 查询参数
$queryParams = array (
    'cardNum' => $cardNum,
);
// body参数（POST方法下）
$bodyParams = array (
);
// url参数拼接
$url = 'https://service-4e2ht5oj-1255468759.ap-beijing.apigateway.myqcloud.com/release/get';
if (count($queryParams) > 0) {
    $url .= '?' . http_build_query($queryParams);
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
curl_setopt($ch, CURLOPT_HTTPHEADER, array_map(function ($v, $k) {
    return $k . ': ' . $v;
}, array_values($headers), array_keys($headers)));
if (in_array($method, array('POST', 'PUT', 'PATCH'), true)) {
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($bodyParams));
}

$data = curl_exec($ch);
if (curl_errno($ch)) {
    echo "Error: " . curl_error($ch);
} else {
    print_r($data);
}
curl_close($ch);